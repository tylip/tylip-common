import sbt.internal.ProjectMatrix
import com.typesafe.tools.mima.core._

val scala213 = "2.13.10"
val scala212 = "2.12.20"

val tylipPublic =
  "tylip-public" at "https://nexus.tylip.com/repository/tylip-public/"
ThisBuild / publishTo := Some(tylipPublic)
ThisBuild / credentials += Credentials(Path.userHome / ".sbt" / ".credentials")

val commonSettings = Seq(
  organization := "com.tylip",
  resolvers += tylipPublic,
  mimaPreviousArtifacts :=
    previousStableVersion.value.map(organization.value %% moduleName.value % _).toSet,
  mimaBinaryIssueFilters ++= Seq(
    ProblemFilters.exclude[IncompatibleMethTypeProblem]("com.tylip.fs2kafka.typed.KafkaTopic.this"),
  ),
  scalafmtOnCompile := !insideCI.value,
  scalacOptions ++= Seq(
    "-deprecation", // Emit warning and location for usages of deprecated APIs.
    "-feature", // Emit warning and location for usages of features that should be imported explicitly.
    "-unchecked", // Enable additional warnings where generated code depends on assumptions.
    "-Ywarn-dead-code", // Warn when dead code is identified.
  ),
  scalacOptions ++= (
    if (insideCI.value) Seq("-Xfatal-warnings")
    else                Seq()
  ),
)

lazy val core = (projectMatrix in file("core"))
  .jvmPlatform(scalaVersions = Seq(scala213, scala212))
  .settings(commonSettings)
  .settings(
    name := "tylip-common-core",
  )

val core212 = disable212Warning(core)

lazy val root = (project in file("."))
  .aggregate(
    core.projectRefs : _*
  )
  .settings(
    publish / skip := true,
    mimaPreviousArtifacts := Set.empty,
  )

def disable212Warning(matrix: ProjectMatrix) =
  matrix.finder()(scala212)
    .settings(
      scalacOptions ++= Seq("-language:higherKinds"),
    )

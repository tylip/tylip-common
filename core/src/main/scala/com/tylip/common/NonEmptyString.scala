package com.tylip.common

import scala.util.Try

class NonEmptyString(val head: Char, val tail: String) {

  lazy val length: Int = value.length

  val value: String = s"$head$tail"
  override lazy val toString: String = value
  override def equals(o: Any): Boolean =
    o match {
      case that: NonEmptyString => that.value == this.value
      case _                    => false
    }
  override lazy val hashCode: Int = value.hashCode
  def +(other: NonEmptyString): NonEmptyString =
    NonEmptyString(head, tail + other.value)
  def +(other: String): NonEmptyString = NonEmptyString(head, tail + other)

  def unsafeTake(n: Int): NonEmptyString =
    NonEmptyString unsafeFrom value.take(n)
  def replace(oldChar: Char, newChar: Char): NonEmptyString =
    NonEmptyString unsafeFrom value.replace(oldChar, newChar)

}

object NonEmptyString {

  private val errorMessage = "Required string is empty"

  /**
   * NonEmptyStringContext allows to use quasiquotes to initialize
   * NonEmptyStrings as nes"this is a non-empty string". NonEmptyStringContext
   * must be imported to make this quasiquote available.
   */
  implicit class NonEmptyStringContext(val sc: StringContext) extends AnyVal {

    def nes(args: Any*): NonEmptyString =
      NonEmptyString.unsafeFrom(sc.s(args: _*))

    /**
     * performs `stripMargin` prior to generating a non-empty string
     */
    def nessm(args: Any*): NonEmptyString =
      NonEmptyString.unsafeFrom(sc.s(args: _*).stripMargin)

  }

  def apply(head: Char, tail: String = "") = new NonEmptyString(head, tail)

  def from(str: String): Option[NonEmptyString] =
    str.headOption.map(NonEmptyString(_, str.tail))

  def eitherStrFrom(string: String): Either[String, NonEmptyString] =
    from(string).toRight(errorMessage)

  def eitherExFrom(
    string: String,
  ): Either[IllegalArgumentException, NonEmptyString] =
    eitherStrFrom(string).left.map(new IllegalArgumentException(_))

  def tryFrom(str: String): Try[NonEmptyString] =
    eitherExFrom(str).toTry

  def unsafeFrom(str: String): NonEmptyString =
    eitherExFrom(str).toTry.get

  def apply(head: Char): NonEmptyString =
    apply(head)

  def apply(str: Int): NonEmptyString = unsafeFrom(str.toString)
  def apply(str: Long): NonEmptyString = unsafeFrom(str.toString)
  def apply(str: Double): NonEmptyString = unsafeFrom(str.toString)
  def apply(str: Boolean): NonEmptyString = unsafeFrom(str.toString)

}

object NonEmptyStrings {
  def unite(
    list: List[NonEmptyString],
    separator: String = ", ",
  ): Option[NonEmptyString] =
    list.foldLeft(None: Option[NonEmptyString]) {
      case (None, neStr)       => Some(neStr)
      case (Some(left), right) => Some(left + separator + right)
    }
}

object AsNes {
  def unapply(str: String): Option[NonEmptyString] =
    NonEmptyString.from(str)
}

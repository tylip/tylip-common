package com.tylip.common

object ModifyFor {

  implicit class ModifyIfNonEmpty[T](val value: T) extends AnyVal {
    def modifiedBy[S](option: Option[S])(f: (S, T) => T): T =
      option.fold[T => T](identity)(f.curried)(value)
    def modifyFor[S](option: Option[S])(f: (T, S) => T): T =
      option.fold[T => T](identity)(s => t => f(t, s))(value)
  }

}
